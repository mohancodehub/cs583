def keep_ordered(my_list):
    # save the element order in a dict:
    x_dict = dict( (y,y) for x,y in enumerate(my_list))
    x_set = set(my_list)
    #perform desired set operations
    print(x_set)
    #retrieve ordered list from the set:
    new_list = [None] * len(x_set)
    count = 0
    for element in x_set:
        new_list[count] = x_dict[element]
        count = count + 1  
    print(new_list)
    

   
if __name__ == '__main__':  print("Inga:",keep_ordered( [1,3,0]))